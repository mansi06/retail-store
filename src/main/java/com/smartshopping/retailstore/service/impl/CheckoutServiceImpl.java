package com.smartshopping.retailstore.service.impl;

import com.smartshopping.retailstore.exception.InvalidResourceValueException;
import com.smartshopping.retailstore.exception.ResourceNotFoundException;
import com.smartshopping.retailstore.model.Product;
import com.smartshopping.retailstore.model.request.ProductRequest;
import com.smartshopping.retailstore.model.response.ProductResponse;
import com.smartshopping.retailstore.service.CheckoutService;
import com.smartshopping.retailstore.utils.ProductLevy;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CheckoutServiceImpl implements CheckoutService {

    private List<String> correctList = Arrays.asList("A", "B", "C");

    @Override
    public ProductResponse getGeneratedBill(ProductRequest productRequest) {

        if(productRequest.getProductList().isEmpty()) {
            throw new InvalidResourceValueException("Invalid Resource exception", productRequest.getProductList().toString());
        }else if(!productRequest.getProductList().stream().anyMatch(element -> correctList.contains(element))) {
            throw new ResourceNotFoundException("Resource not found exception", productRequest.getProductList().toString());
        }

        double totalCost = 0.0;
        double totaltax = 0.0;

        for(String prod: productRequest.getProductList()) {
            totalCost += getTotalCost(prod);
            totaltax += getTotalTax(prod);
        }

        return ProductResponse.builder()
                       .totalCost(totalCost)
                       .totalTax(totaltax)
                       .productList(productRequest.getProductList().stream()
                                        .map(strings -> ProductLevy.getProductCost(strings))
                                        .collect(Collectors.toList())
                       ).build();
    }

    private double getTotalTax(String s) {
        Product product = ProductLevy.getProductCost(s);
        return (product.getPrice() * product.getLevy());
    }

    private double getTotalCost(String s) {
        Product product = ProductLevy.getProductCost(s);
        return (product.getPrice() + product.getPrice() * product.getLevy());
    }

}
