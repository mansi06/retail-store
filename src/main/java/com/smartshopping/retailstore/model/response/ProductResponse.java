package com.smartshopping.retailstore.model.response;

import com.smartshopping.retailstore.model.Product;
import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class ProductResponse {
    private double totalCost;
    private double totalTax;
    private List<Product> productList;
}
