package com.smartshopping.retailstore.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.smartshopping.retailstore.exception.InvalidResourceValueException;
import com.smartshopping.retailstore.exception.ResourceNotFoundException;
import com.smartshopping.retailstore.model.Product;
import com.smartshopping.retailstore.model.request.ProductRequest;
import com.smartshopping.retailstore.model.response.ProductResponse;
import com.smartshopping.retailstore.service.CheckoutService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.Arrays;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.skyscreamer.jsonassert.JSONAssert.assertEquals;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CheckoutController.class)
public class CheckoutControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private CheckoutService checkoutService;

    private static final String recipt_url= "/retail-store/v1/products/generateBill";

    private JacksonTester<ProductRequest> jsonTester;
    private ProductRequest productRequest;
    private ProductResponse productResponse;

    @Before
    public void setUp() throws Exception {
        JacksonTester.initFields(this, objectMapper);

    }

    private ProductResponse buildProductResponse() {
        return ProductResponse.builder()
                        .totalCost(350)
                        .totalTax(50)
                        .productList(Arrays.asList(getProductA(), getProductB()))
                .build();
    }

    private Product getProductB() {
        return new Product("B101", "B", 200.0, 0.2);
    }

    private Product getProductA() {
        return new Product("A101", "A", 100.0, 0.1);
    }

    @Test
    public void generateBill_returnProductResponse() throws Exception{
        String[] sarray = {"A", "B"};
        productRequest = new ProductRequest(Arrays.asList(sarray));
        productResponse = buildProductResponse();
        final String productRequestJson = jsonTester.write(productRequest).getJson();
        when(checkoutService.getGeneratedBill(any())).thenReturn(productResponse);

        ResultActions response =
                mockMvc.perform(post(recipt_url)
                       .content(productRequestJson).contentType(APPLICATION_JSON))
                       .andExpect(status().isOk());

        JSONAssert.assertEquals(
                getJsonDetails(), response.andReturn().getResponse().getContentAsString(), true);
    }

    @Test
    public void itShouldGetNotFoundStatus_WhenWrongItemIsFound() throws Exception{
        String[] invalidArray={"D","E"};

        String message = "Resource not found exception";
        int code = HttpStatus.NOT_FOUND.value();
        String error = "RESOURCE_NOT_FOUND - " + Arrays.toString(invalidArray);

        ProductRequest invalidProductRequest=new ProductRequest(Arrays.asList(invalidArray));
        final String invalidProductRequestJson = jsonTester.write(invalidProductRequest).getJson();
        when(checkoutService.getGeneratedBill(invalidProductRequest)).thenThrow(new ResourceNotFoundException("Resource not found exception", Arrays.toString(invalidArray)));
        ResultActions response =
                mockMvc.perform(post(recipt_url)
                .content(invalidProductRequestJson).contentType(APPLICATION_JSON))
                .andExpect(status().isNotFound());
        assertEquals(
                getErrorJsonDetails(message, code, error),
                response.andReturn().getResponse().getContentAsString(),
                true);
    }

    @Test
    public void itShouldGetBadStatus_WhenNoItemIsFound() throws Exception{
        String[] invalidArray={};
        String message =
                String.format("Invalid Resource exception", invalidArray);
        int code = HttpStatus.BAD_REQUEST.value();
        String error = "INVALID_RESOURCE - " + Arrays.toString(invalidArray);

        ProductRequest invalidProductRequest=new ProductRequest(Arrays.asList(invalidArray));
        final String invalidProductRequestJson = jsonTester.write(invalidProductRequest).getJson();
        when(checkoutService.getGeneratedBill(invalidProductRequest)).thenThrow(new InvalidResourceValueException("Invalid Resource exception", Arrays.toString(invalidArray)));

        ResultActions response= mockMvc.perform(post(recipt_url)
                .content(invalidProductRequestJson).contentType(APPLICATION_JSON))
               .andExpect(status().isBadRequest());

        assertEquals(
                getErrorJsonDetails(message, code, error),
                response.andReturn().getResponse().getContentAsString(),
                true);

    }

    private String getJsonDetails() {
        return("{\n" +
                "  \"totalCost\": 350,\n" +
                "  \"totalTax\": 50,\n" +
                "  \"productList\": [\n" +
                "    {\n" +
                "      \"productCode\": \"A101\",\n" +
                "      \"name\": \"A\",\n" +
                "      \"price\": 100,\n" +
                "      \"levy\": 0.1\n" +
                "    },\n" +
                "    {\n" +
                "      \"productCode\": \"B101\",\n" +
                "      \"name\": \"B\",\n" +
                "      \"price\": 200,\n" +
                "      \"levy\": 0.2\n" +
                "    }\n" +
                "  ]\n" +
                "}");
    }

    private String getErrorJsonDetails(String message, int code, String error) {
        return ("{\n"
                + "   \"error\":{\n"
                + "         \"message\":\""
                + message
                + "\",\n"
                + "         \"code\":\""
                + code
                + "\",\n"
                + "         \"error\":\""
                + error
                + "\"\n"
                + "      }\n"
                + "}");
    }
}