package com.smartshopping.retailstore.service;

import com.smartshopping.retailstore.model.request.ProductRequest;
import com.smartshopping.retailstore.model.response.ProductResponse;

public interface CheckoutService {
    ProductResponse getGeneratedBill(ProductRequest productRequest);
}
