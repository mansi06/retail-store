package com.smartshopping.retailstore.utils;

import com.smartshopping.retailstore.model.Product;

public class ProductLevy {

    public static Product getProductCost(String productName) {

        Product product = new Product();

        switch (productName) {
            case "A":
                product = new Product("A101", "A", 100.0, 0.1);
                break;
            case "B":
                product = new Product("B101", "B", 200.0, 0.2);
                break;
            case "C":
                product = new Product("C101", "C", 300.0, 0);
            break;
            default:
                throw new IllegalArgumentException("Invalid Product value: " + productName);
        }
        return product;
    }
}

