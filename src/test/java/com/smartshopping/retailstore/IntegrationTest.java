package com.smartshopping.retailstore;

import io.micrometer.core.instrument.util.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.skyscreamer.jsonassert.JSONAssert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IntegrationTest {

    private static final String BILLING_RESOURCE_URL = "/retail-store/v1/products/generateBill";

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getProductResponse_whenCorrectProductListIsSent() throws Exception {
        String entryString = getFileContent("json_request/correct-product-list.json");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity<String> request = new HttpEntity<>(entryString, httpHeaders);

        ResponseEntity<String> response =
                restTemplate.postForEntity(BILLING_RESOURCE_URL, request, String.class);

        String expectedJson = getFileContent("json_response/correct-product-response.json");
        final String actualJson = response.getBody();

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertEquals(expectedJson, actualJson, true);
    }

    @Test
    public void itShouldGetNotFoundStatus_WhenWrongItemIsFound() throws Exception {
        String entryString = getFileContent("json_request/wrong-product-list.json");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity<String> request = new HttpEntity<>(entryString, httpHeaders);

        ResponseEntity<String> response =
                restTemplate.postForEntity(BILLING_RESOURCE_URL, request, String.class);

        String expectedJson = getFileContent("json_response/error-product-not-present.json");
        final String actualJson = response.getBody();

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertEquals(expectedJson, actualJson, true);
    }

    @Test
    public void itShouldGetBadRequestStatus_WhenNoItemIsPassed() throws Exception {
        String entryString = getFileContent("json_request/empty-product-list.json");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity<String> request = new HttpEntity<>(entryString, httpHeaders);

        ResponseEntity<String> response =
                restTemplate.postForEntity(BILLING_RESOURCE_URL, request, String.class);

        String expectedJson = getFileContent("json_response/error-product-list-empty.json");
        final String actualJson = response.getBody();

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertEquals(expectedJson, actualJson, true);
    }

    static String getFileContent(String file) {
        try {
            return IOUtils.toString(new ClassPathResource(file).getInputStream());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
