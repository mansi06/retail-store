package com.smartshopping.retailstore.service;

import com.smartshopping.retailstore.exception.InvalidResourceValueException;
import com.smartshopping.retailstore.exception.ResourceNotFoundException;
import com.smartshopping.retailstore.model.Product;
import com.smartshopping.retailstore.model.request.ProductRequest;
import com.smartshopping.retailstore.model.response.ProductResponse;
import com.smartshopping.retailstore.service.impl.CheckoutServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class CheckoutServiceTest {

    private CheckoutService checkoutService;
    private ProductRequest productRequest;
    private ProductResponse productResponse;

    @Before
    public void setUp() {
        checkoutService = new CheckoutServiceImpl();
        String[] sarray = {"A", "B"};
        productRequest = new ProductRequest(Arrays.asList(sarray));
        productResponse = buildProductResponse();
    }

    private ProductResponse buildProductResponse() {
        return ProductResponse.builder()
                              .totalCost(350)
                              .totalTax(50)
                              .productList(Arrays.asList(getProductA(), getProductB()))
                              .build();
    }

    private Product getProductB() {
        return new Product("B101", "B", 200.0, 0.2);
    }

    private Product getProductA() {
        return new Product("A101", "A", 100.0, 0.1);
    }

    @Test
    public void getGeneratedBill_shouldReturnBillRecipt() {
        assertEquals(checkoutService.getGeneratedBill(productRequest).getTotalCost(),productResponse.getTotalCost(), 0.001);
        assertEquals(checkoutService.getGeneratedBill(productRequest).getTotalTax(),productResponse.getTotalTax(), 0.001);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void itShouldGetNotFoundStatus_WhenWrongItemIsFound() throws Exception{
        String[] iarray = {"D", "E"};
        productRequest = new ProductRequest(Arrays.asList(iarray));
        checkoutService.getGeneratedBill(productRequest);
    }

    @Test(expected = InvalidResourceValueException.class)
    public void itShouldGetBadStatus_WhenNoItemIsFound() throws Exception{
        String[] iarray = {};
        productRequest = new ProductRequest(Arrays.asList(iarray));
        checkoutService.getGeneratedBill(productRequest);
    }
}