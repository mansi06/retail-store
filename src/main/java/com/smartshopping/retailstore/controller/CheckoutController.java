package com.smartshopping.retailstore.controller;

import com.smartshopping.retailstore.model.request.ProductRequest;
import com.smartshopping.retailstore.model.response.ProductResponse;
import com.smartshopping.retailstore.service.CheckoutService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/retail-store/v1/products")
@RequiredArgsConstructor
public class CheckoutController {

    private final CheckoutService checkoutService;

    @PostMapping("/generateBill")
    public ResponseEntity<ProductResponse> generateBill(@RequestBody ProductRequest productRequest) {
        ProductResponse generatedBill = checkoutService.getGeneratedBill(productRequest);
        return ResponseEntity.ok().body(generatedBill);
    }
}