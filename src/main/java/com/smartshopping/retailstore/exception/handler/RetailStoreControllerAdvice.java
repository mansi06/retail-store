package com.smartshopping.retailstore.exception.handler;

import com.smartshopping.retailstore.exception.ErrorDetails;
import com.smartshopping.retailstore.exception.InvalidResourceValueException;
import com.smartshopping.retailstore.exception.ResourceNotFoundException;
import com.smartshopping.retailstore.model.response.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestControllerAdvice
@Slf4j
public class RetailStoreControllerAdvice {

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ErrorResponse> resourceNotFound(ResourceNotFoundException ex) {
        log.error(String.format("Resource not found exception %s",ex.getMessage(),ex));
        ErrorResponse errorResponse=buildErrorResponse(ex.getMessage(),NOT_FOUND.value(),"RESOURCE_NOT_FOUND - " + ex.getError());
        return ResponseEntity.status(NOT_FOUND).body(errorResponse);
    }

    @ExceptionHandler(InvalidResourceValueException.class)
    public ResponseEntity<ErrorResponse> invalidFieldException(InvalidResourceValueException ex) {
        log.error(String.format("Invalid Resource exception %s", ex.getMessage()), ex);
        ErrorResponse errorResponse = buildErrorResponse(ex.getMessage(), BAD_REQUEST.value(), "INVALID_RESOURCE - " + ex.getError());
        return ResponseEntity.badRequest().body(errorResponse);
    }

    private ErrorResponse buildErrorResponse(String message, int errorCode,String error) {
        return ErrorResponse.builder()
                            .error(buildError(message, errorCode,error))
                            .build();
    }

    private ErrorDetails buildError(String message, int errorCode, String error) {
        return ErrorDetails.builder()
                           .message(message)
                           .code(String.valueOf(errorCode))
                           .error(error)
                           .build();
    }


}
