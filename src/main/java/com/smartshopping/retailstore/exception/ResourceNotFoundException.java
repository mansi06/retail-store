package com.smartshopping.retailstore.exception;

import lombok.Getter;

public class ResourceNotFoundException extends RuntimeException {

    @Getter
    private String error;

    public ResourceNotFoundException(String message, String error) {
        super(message);
        this.error = error;
    }
}
