package com.smartshopping.retailstore.exception;

import lombok.Getter;

public class InvalidResourceValueException extends RuntimeException {

    @Getter
    private String error;

    public InvalidResourceValueException(String message,String error) {
        super(message);
        this.error=error;
    }
}
