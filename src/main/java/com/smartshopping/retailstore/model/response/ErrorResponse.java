package com.smartshopping.retailstore.model.response;

import com.smartshopping.retailstore.exception.ErrorDetails;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class ErrorResponse {

    private ErrorDetails error;
}
