package com.smartshopping.retailstore.utils;

import com.smartshopping.retailstore.model.Product;
import org.junit.Test;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class ProductLevyTest {

    @Test
    public void getProductCost() {
        //given
        String productName = "A";

        //when
        Product productA = ProductLevy.getProductCost(productName);

        //then
        assertThat(productA.getName()).isEqualTo(productName);
        assertThat(productA.getProductCode()).isEqualTo("A101");
        assertThat(productA.getPrice()).isEqualTo(100.0);
        assertThat(productA.getLevy()).isEqualTo(0.1);

    }

    @Test(expected = IllegalArgumentException.class)
    public void getIllegalArgumentException_WhenWrongProductIsSent() {
        //given
        String productName = "X";

        //when
        Product productA = ProductLevy.getProductCost(productName);
    }
}